# String Permutations

A .NET-Core example with dotnet test

## Setup
Include both projects in `global.json`. 

### Create the test project
In `test\Permutation.Test`:
```
dotnet new -t xunittest
dotnet restore
```

### Create the main project
In `src\Permutation`:
```
dotnet new
dotnet restore
```

## Run test
```
dotnet test
```

Find instructions [here](https://docs.microsoft.com/en-us/dotnet/articles/core/testing/unit-testing-with-dotnet-test).

