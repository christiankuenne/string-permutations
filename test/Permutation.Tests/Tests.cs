﻿using Xunit;
using Permutations;

namespace Tests
{
    public class Tests
    {
        [Fact]
        public void TestPermutation() 
        {
            Permutation p = new Permutation("abc");
            Assert.Equal("abc,acb,bac,bca,cab,cba", p.DoPermutation());
        }
    }
}
