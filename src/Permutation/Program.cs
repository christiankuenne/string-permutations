﻿using System;

namespace Permutations
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Permutation p = new Permutation("Klaus");
            Console.WriteLine(p.DoPermutation());
        }
    }
}
