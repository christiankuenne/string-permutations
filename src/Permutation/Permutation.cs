using System;
using System.Collections.Generic;

namespace Permutations {

    public class Permutation {

        private String input;
        private IList<String> output;

        public Permutation(String word) {
            this.input = word;            
        }

        public String DoPermutation() {
            this.output = new List<String>();        
            DoPermutationRec(this.input, "");
            return String.Join(",", this.output);
        }

        private void DoPermutationRec(String word, String prefix) {
            if (word.Length < 2) {
                this.output.Add(prefix + word);
                return;
            }
            for (int i = 0; i < word.Length; ++i) {
                DoPermutationRec(word.Remove(i, 1), prefix + word[i].ToString());
            }
        }
    }
}
